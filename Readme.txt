------------ Thank you for your purchase! -----------

To get started, open the documentation folder double click the link file. 
You can also browse to http://sherdle.com/support/documentation/

----------- License -------------

For more license information, check the License.txt file inside the template's folder.

------------ Content -----------

- Your template
- Wordpress plugin (fork)
- Documentation

———————————— Upgrading —————————

V2.0
Rewritten with StoryBoard & Autolayout
Support for all devices (iPad mini, iPad air, iPad, iPad Pro, iPhone plus)
Support for Split-View
Improved for IOS9
In-App background radio playing
Bug fixes

Upgrade instructions:
Only the config.m, appdelegate.h file and image resources (including assets) can be maintained.

V1.2
Admob Support
Bugfixes
IOS9 (Beta) & XCode 7 (Beta) Support

Upgrade instructions:
Only the config.m file and image resources (including assets) can be maintained.

V1.1
IAd Support

Upgrade instructions:
Only the config.m file and image resources (including assets) can be maintained.

V1.0
Initial release

