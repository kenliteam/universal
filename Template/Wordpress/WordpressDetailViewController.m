//
//  WordpressDetailViewController.m
//
//  Copyright (c) 2015 Sherdle. All rights reserved.
//

#import "WordpressDetailViewController.h"
#import "AppDelegate.h"
#import "UIImageView+WebCache.h"
#import "NSString+HTML.h"
#import "FrontNavigationController.h"
#import "UIViewController+PresentActions.h"

#define LABEL_WIDTH self.articleDetail.tableView.frame.size.width - 26

@implementation WordpressDetailViewController
{
    ShowPageCell *contentCell;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    self.articleDetail.tableViewDataSource = self;
    self.articleDetail.tableViewDelegate = self;
    
    self.articleDetail.delegate = self;
    self.articleDetail.parallaxScrollFactor = 0.3; // little slower than normal.
    
    self.view.clipsToBounds = YES;
    
    
    NSString *authorAddition = @"";
    if ([_author length] > 0){
        authorAddition = [NSString stringWithFormat:@"written by %@", _author];
    }
    
    _subTitleText = [NSString stringWithFormat:@"Published on %@ %@", _date, authorAddition];

    NSString *strURl=[NSString stringWithFormat:@"%@get_post/?post_id=%@",_mainURL,_detailID];
    //NSLog(@"%@get_post/?post_id=%@", _mainURL,_detailID);
    
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:strURl]];
    
    _html = @"";
    
    [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        // [self performSelectorOnMainThread:@selector(hideProgressForView:) withObject:self.navigationController.view waitUntilDone:NO];
        if (data == nil) {
            [[[UIAlertView alloc]initWithTitle:@"Error" message:NO_CONNECTION_TEXT delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            
            return ;
        }
        else  {
            
            NSDictionary *jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&connectionError];
            NSArray *jsonArray = [jsonDict valueForKey:@"post"];
            
            //NSLog(@"json result %@ ",[jsonArray valueForKey:@"title"]);
            //NSLog(@"json dict  %@ ",[[jsonDict valueForKey:@"title"]valueForKey:@"title"]);
            //NSLog(@"jsonArray = %@",jsonArray);
            
            //NSString *itemTitle = [jsonArray valueForKey:@"title"];
            //NSString *date= [jsonArray valueForKey:@"date"];
            
            _html =  [jsonArray valueForKey:@"content"]; // the post content
            
            //TODO Removing the first image, might not always be necesairy!
            
            NSString *regexStr = @"<img ([^>]+)>";
            
            NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regexStr options:NSRegularExpressionCaseInsensitive error:NULL];
            NSRange range = [regex rangeOfFirstMatchInString:_html options:kNilOptions range:NSMakeRange(0, [_html length])];

            if (range.location != NSNotFound){
                _html = [regex stringByReplacingMatchesInString:_html options:0 range:range withTemplate:@"$2"];
            }
            
            NSIndexPath* indexPath1 = [NSIndexPath indexPathForRow:1 inSection:0];
            // Add them in an index path array
            NSArray* indexArray = [NSArray arrayWithObjects:indexPath1, nil];
            // Launch reload for the two index path
            [self.articleDetail.tableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationFade];
        }
        
    }];

    if (_imageUrl) {
        [self.articleDetail.imageView sd_setImageWithURL:[NSURL URLWithString:_imageUrl] placeholderImage:[UIImage imageNamed:@"default_placeholder"]];
        self.articleDetail.imageView.contentMode = UIViewContentModeScaleAspectFill;
    } else {
        // No header image? Hide the image top view.
        self.articleDetail.noImage = YES;
        
        //make the header appear
        FrontNavigationController *nc = (FrontNavigationController *)self.navigationController;
        [nc.gradientView turnTransparencyOn:NO animated:NO];
    }

    // after setting the above properties
    [self.articleDetail initialLayout];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"articleDetail"]) {
        self.articleDetail = (DetailViewAssistant *)segue.destinationViewController.view;
        self.articleDetail.parentController = segue.destinationViewController;
    }
}

#pragma mark - UITableView

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 1) {
        return contentCell ? [contentCell updateWebViewHeightForWidth:tableView.frame.size.width] : 50.0f;
    }

    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{    
    if (indexPath.row == 0) {
        TitleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"titleCell" forIndexPath:indexPath];
        
        cell.lblTitle.text = [_titleText stringByDecodingHTMLEntities];
        cell.lblDescription.text = _subTitleText;

        return cell;
    }
    else if (indexPath.row == 1) {
        contentCell = (ShowPageCell *)[tableView dequeueReusableCellWithIdentifier:@"ShowPageCell" forIndexPath:indexPath];

        contentCell.parentTable = [self.articleDetail getTableView];
        [contentCell loadContent:_html];
        
        return contentCell;
    }
    else if (indexPath.row == 2) {
        ActionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"actionCell" forIndexPath:indexPath];
        cell.actionDelegate = self;

        return cell;
    }
    else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"reusable"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"reusable"];
        }
        
        cell.textLabel.text = @"Default cell";
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell.contentView.backgroundColor = [UIColor whiteColor];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat scrollOffset = scrollView.contentOffset.y;
    
    // let the view handle the paralax effect
    [self.articleDetail scrollViewDidScrollWithOffset:scrollOffset];

    if (!self.articleDetail.noImage) {
        // switch the nav bar opaque/transparent at the threshold
        FrontNavigationController *nc = (FrontNavigationController *)self.navigationController;
        [nc.gradientView turnTransparencyOn:(scrollOffset < self.articleDetail.headerFade) animated:YES];
    }
}

#pragma mark - UIContentContainer Protocol

- (void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
    
    [contentCell updateWebViewHeightForWidth:size.width];
}

#pragma mark - Button actions

- (void)open
{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",_articleUrl]]];
}

- (IBAction)share:(id)sender
{
    NSArray *activityItems = [NSArray arrayWithObjects:_articleUrl,  nil];

    [self presentActions:activityItems sender:(id)sender];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
