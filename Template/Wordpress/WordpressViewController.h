//
// WordpressViewController.h
//
// Copyright (c) 2015 Sherdle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STableViewController.h"
#import "UIImageView+WebCache.h"
#import "MWFeedParser.h"

@interface WordpressViewController : STableViewController {
  
    NSMutableArray *parsedItems;
    
    NSDateFormatter *formatter;
    int page;
    NSDictionary *jsonDict;

}

@property(strong,nonatomic)NSString *urlString;
@property(strong,nonatomic)NSString *mainURL;
@property(strong,nonatomic)NSString *navTitle;

@end
