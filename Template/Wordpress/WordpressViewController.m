//
// WordpressViewController.m
//
// Copyright (c) 2015 Sherdle. All rights reserved.
//


#import "WordpressViewController.h"
#import "AssistTableCell.h"
#import "AssistTableHeaderView.h"
#import "AssistTableFooterView.h"

#import "NSString+HTML.h"
#import "MWFeedParser.h"

#import "WordpressDetailViewController.h"
#import "SWRevealViewController.h"
#import "KILabel.h"
#import "AppDelegate.h"
#import "CommonBanner.h"
#import "UITableView+FDTemplateLayoutCell.h"

#define HEADERVIEW_HEIGHT 175
#define WORDPRESSCORRECTION true

@implementation WordpressViewController
@synthesize urlString, mainURL;

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 166;
    
    if (ADS_ON)
        self.canDisplayAds = YES;
    
    [self.tableView addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    [self.tableView addGestureRecognizer: self.revealViewController.tapGestureRecognizer];
    
    if (WORDPRESSCORRECTION)
        page = -1;
    
    self.title = @"Loading...";
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}


-(void) fetchDataWith:(NSString *)withCount intValue:(int) setValue
{
    NSString *strURl;
    
    if ([urlString isEqualToString:@"latest"])
    {
        strURl=[NSString stringWithFormat:@"%@get_recent_posts/?page=%i&dev=1",mainURL,page];
    }
    else{
        
        strURl=[NSString stringWithFormat:@"%@get_category_posts/?page=%i&slug=%@&dev=1",mainURL,page,urlString];
    }
    
    if (urlString.length == 0 )
    {
        strURl=[NSString stringWithFormat:@"%@get_recent_posts/?page=%i&dev=1",mainURL,page];
    }
    NSLog(@"%@",strURl);
    
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:strURl]];
    
    
    [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        // [self performSelectorOnMainThread:@selector(hideProgressForView:) withObject:self.navigationController.view waitUntilDone:NO];
        if (data == nil) {
            [[[UIAlertView alloc]initWithTitle:@"Error" message:NO_CONNECTION_TEXT delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            
            [self refreshCompleted];
            [self loadMoreCompleted];
            
            self.title = _navTitle;
            
            return ;
        }
        else  {
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                
                jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                
                NSArray *jsonArray = [jsonDict valueForKey:@"posts"];
                
                //NSLog(@"jsonArray = %@",jsonArray);
                
                if (!parsedItems){
                    parsedItems = [[NSMutableArray alloc]init];
                }
                for (NSArray *resultkey in jsonArray) {
                    NSMutableArray *result = [resultkey mutableCopy];
                    
                    //TODO, search for thumbnail in specific to store and use seperately.
                    NSString *htmlString = [result valueForKey:@"excerpt"];
                    NSString *url = @"";
                    NSArray *attachments= [result valueForKey:@"attachments"];
                    NSObject *thumbnails= [result valueForKey:@"thumbnail_images"];
                    if ([attachments count] > 0 && [NSNull null] != [[attachments objectAtIndex:0] valueForKey:@"url"]){
                        url= [[attachments objectAtIndex:0] valueForKey:@"url"];
                    } else if ([NSNull null] != thumbnails && [thumbnails valueForKey:@"medium"] != nil){
                        url= [[thumbnails valueForKey:@"medium"] valueForKey:@"url"];
                    } else {
                        NSScanner *theScanner = [NSScanner scannerWithString:htmlString];
                        // find start of IMG tag
                        [theScanner scanUpToString:@"<img" intoString:nil];
                        if (![theScanner isAtEnd]) {
                            [theScanner scanUpToString:@"src" intoString:nil];
                            NSCharacterSet *charset = [NSCharacterSet characterSetWithCharactersInString:@"\"'"];
                            [theScanner scanUpToCharactersFromSet:charset intoString:nil];
                            [theScanner scanCharactersFromSet:charset intoString:nil];
                            [theScanner scanUpToCharactersFromSet:charset intoString:&url];
                        }
                        // "url" now contains the URL of the img
                    }
                    
                    if (url == (id)[NSNull null]){
                        url = @"";
                    }
                    
                    [result setValue:url forKey:@"mediaUrl"];
                    
                    NSString *authorname = @"";
                    NSObject *author;
                    
                    if ([result valueForKey:@"author"] != nil){
                        if ([[result valueForKey:@"author"] isKindOfClass:[NSArray class]] && [[result valueForKey:@"author"] count] > 0){
                            author = [[result valueForKey:@"author"] objectAtIndex:0];
                        } else {
                            author = [result valueForKey:@"author"];
                        }
                        
                        if ([author isKindOfClass:[NSObject class]] && [author valueForKey:@"name"] != nil){
                            authorname = [author valueForKey:@"name"];
                        }
                    }
                    
                    [result setValue:authorname forKey:@"author"];
                    
                    [parsedItems addObject:result];
                }
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.title= _navTitle;
                    
                    [self.tableView performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
                    
                    [self loadMoreCompleted];
                    [self refreshCompleted];
                });
            });
            
        }
        
    }];
    
}

- (void) pinHeaderView
{
    [super pinHeaderView];
    
    // do custom handling for the header view
    AssistTableHeaderView *hv = (AssistTableHeaderView *)self.headerView;
    [hv.activityIndicator startAnimating];
    hv.title.text = @"Loading...";
}

- (void) unpinHeaderView
{
    [super unpinHeaderView];
    
    // do custom handling for the header view
    [[(AssistTableHeaderView *)self.headerView activityIndicator] stopAnimating];
}

- (void) headerViewDidScroll:(BOOL)willRefreshOnRelease scrollView:(UIScrollView *)scrollView
{
    AssistTableHeaderView *hv = (AssistTableHeaderView *)self.headerView;
    if (willRefreshOnRelease)
        hv.title.text = @"Release to refresh...";
    else
        hv.title.text = @"Pull down to refresh...";
}


- (BOOL) refresh
{
    if (![super refresh])
        return NO;
    
    //TODO implement real time
    [self performSelector:@selector(addItemsOnTop) withObject:nil afterDelay:2.0];
    // See -addItemsOnTop for more info on how to finish loading
    return YES;
}

- (void) willBeginLoadingMore
{
    AssistTableFooterView *fv = (AssistTableFooterView *)self.tableView.tableFooterView;
    [fv.activityIndicator startAnimating];
}

- (void) loadMoreCompleted
{
    [super loadMoreCompleted];
    
    AssistTableFooterView *fv = (AssistTableFooterView *)self.tableView.tableFooterView;
    [fv.activityIndicator stopAnimating];
    
    if (!self.canLoadMore) {
        // Do something if there are no more items to load
        
        // Just show a textual info that there are no more items to load
        fv.infoLabel.hidden = NO;
    }
}

- (BOOL) loadMore
{
    if (![super loadMore])
        return NO;
    
    // Do your async loading here
    [self performSelector:@selector(addItemsOnBottom) withObject:nil afterDelay:2.0];
    // See -addItemsOnBottom for more info on what to do after loading more items
    
    return YES;
}

- (void) addItemsOnTop
{
    parsedItems = nil;
    page = 0;
    [self fetchDataWith:urlString intValue:1];
    
    // Call this to indicate that we have finished "refreshing".
    // This will then result in the headerView being unpinned (-unpinHeaderView will be called).
    [self refreshCompleted];
}

- (void) addItemsOnBottom
{
    NSString *total;
    
    total=[NSString stringWithFormat:@"%@",[jsonDict valueForKey:@"pages"]];
    
    page=page+1;
    if (page == 1 && WORDPRESSCORRECTION){
        page = 2;
    }
    
    [self fetchDataWith:urlString intValue:page];
    int totalpage=(int)[total integerValue];
    
    if (page > totalpage && jsonDict){
        self.canLoadMore = NO;
    }
    // signal that there won't be any more items to load
    else
        self.canLoadMore = YES;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return parsedItems.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AssistTableCell *cell;
    
    if ([[[parsedItems objectAtIndex:indexPath.row]valueForKey:@"mediaUrl"] length] != 0 && indexPath.row == 0){
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"HeaderCell" forIndexPath:indexPath];
        [self configureHeaderCell:cell atIndexPath:indexPath];
    } else {
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        [self configureCell:cell atIndexPath:indexPath];
    }
    
    return cell;
}

- (void)configureHeaderCell:(AssistTableCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSString *url = [[parsedItems objectAtIndex:indexPath.row]valueForKey:@"mediaUrl"];
    [cell.image sd_setImageWithURL:[NSURL URLWithString:url]
                  placeholderImage:[UIImage imageNamed:@"default_placeholder"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
    
    cell.lblHeadTitle.text = [[[parsedItems objectAtIndex:indexPath.row]valueForKey:@"title"] stringByDecodingHTMLEntities];
}

- (void)configureCell:(AssistTableCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    NSString *title = [[[parsedItems objectAtIndex:indexPath.row]valueForKey:@"title"] stringByDecodingHTMLEntities];
    NSString *summary = [[parsedItems objectAtIndex:indexPath.row]valueForKey:@"excerpt"];
    NSString *date = [[parsedItems objectAtIndex:indexPath.row]valueForKey:@"date"];
    
    cell.lblTitle.text = title;
    
    NSString *itemSummary = [summary stringByConvertingHTMLToPlainText] ;
    cell.lblSummary.text = itemSummary;
    
    if (date) {
        cell.lblDate.text = date;
    }
    
    if ([[[parsedItems objectAtIndex:indexPath.row]valueForKey:@"mediaUrl"] length] > 0){
        [cell setNoImage:NO];
        
        NSString *url = [[parsedItems objectAtIndex:indexPath.row]valueForKey:@"mediaUrl"];
        [cell.image sd_setImageWithURL:[NSURL URLWithString:url]
                      placeholderImage:[UIImage imageNamed:@"default_placeholder"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
    } else {
        [cell setNoImage:YES];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showDetails"]) {
        NSArray *item = sender;
        
        WordpressDetailViewController *detailVC = (WordpressDetailViewController *)segue.destinationViewController;
        detailVC.titleText = [item valueForKey:@"title"];
        detailVC.detailID = [item valueForKey:@"id"];
        detailVC.articleUrl = [item valueForKey:@"url"];
        detailVC.date = [item valueForKey:@"date"];
        detailVC.author = [item valueForKey:@"author"];
        detailVC.imageUrl = [item valueForKey:@"mediaUrl"];
        
        detailVC.mainURL = mainURL;
        if (detailVC.mainURL.length == 0) {
            detailVC.mainURL = mainURL;
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *item = [parsedItems objectAtIndex:indexPath.row];
    // this check prevents the bug where parsedItems is nil for a second after the pull-down refresh
    if (item) {
        [self performSegueWithIdentifier:@"showDetails" sender:item];
    }
    
    // Deselect
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (void)dealloc
{
    if ([self isViewLoaded])
    {
        self.tableView.delegate = nil;
        self.tableView.dataSource = nil;
    }
}

@end
