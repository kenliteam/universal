//
//  SocialFetcher.h
//
//  Copyright (c) 2015 Sherdle. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum
{
    PageType_NEWSFEED=0,
    PageType_INSTAGRAM=1,
    PageType_TWITTER=3
} PageType;

@interface SocialFetcher : NSObject

+ (NSDictionary *)executeFetch:(NSString *)query;

@end
