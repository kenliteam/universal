//
//  SocialFetcher.m
//
//  Copyright (c) 2015 Sherdle. All rights reserved.
//

#import "SocialFetcher.h"

#define TYPE @"posts" //

@implementation SocialFetcher

+ (NSDictionary *)executeFetch:(NSString *)query
{
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:[query stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]];
    
    NSLog(@"Query: %@", [query stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
    
    NSError *error = nil;
    
    NSData *theData = [NSURLConnection sendSynchronousRequest:request
                                            returningResponse:nil
                                                        error:&error];
    
    if (theData != nil) {
    
        NSDictionary *newJSON = [NSJSONSerialization JSONObjectWithData:theData
                                                            options:0
                                                              error:nil];
        
       return newJSON;
    } else {

        NSLog(@"Error %@", error);
        return nil;
    }
    
    
}

@end
