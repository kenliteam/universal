//
// AssistTableViewController.m
//
// Copyright (c) 2015 Sherdle. All rights reserved.
//

#import "YoutubeViewController.h"
#import "AssistTableCell.h"
#import "AssistTableHeaderView.h"
#import "AssistTableFooterView.h"

#import "NSString+HTML.h"
#import "MWFeedParser.h"
#import "KILabel.h"
#import "SWRevealViewController.h"
#import "FrontNavigationController.h"

#import "YoutubeDetailViewController.h"
#import "AppDelegate.h"

#import "CommonBanner.h"

#define HEADERVIEW_HEIGHT 175

@interface YoutubeViewController ()
// Private helper methods
- (void) addItemsOnTop;
- (void) addItemsOnBottom;

@end

@implementation YoutubeViewController
@synthesize urlString;

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 82;
    
    if (ADS_ON)
        self.canDisplayAds = YES;
    
    formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateStyle:NSDateFormatterShortStyle];
    [formatter setTimeStyle:NSDateFormatterShortStyle];
    
    [self.tableView addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    [self.tableView addGestureRecognizer: self.revealViewController.tapGestureRecognizer];
    
    self.title = @"Loading...";
    
    // add sample items
    [self firstFetchdata];
}


- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)firstFetchdata
{
    parsedItems = [[NSMutableArray alloc]init];
    count=15;
    
    //    [self fetchdata:urlString IntValue:count];
    NSString *str;
    
    str=[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=%@&maxResults=%i&key=%@",urlString,15,YOUTUBE_CONTENT_KEY];
    
    NSLog(@"%@", str);
    NSURL *url = [[NSURL alloc]initWithString:[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc]initWithURL:url];
    
    
    [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (data == nil) {
            [[[UIAlertView alloc]initWithTitle:@"Error" message:NO_CONNECTION_TEXT delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            
            self.title= _navTitle;
            [self loadMoreCompleted];
            [self refreshCompleted];
            
            return ;
        }
        else  {
            jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&connectionError];
            NSArray *jsonArray = [jsonDict valueForKey:@"items"];
            pageToken=[jsonDict valueForKey:@"nextPageToken"];
            
            //            NSLog(@"%@", jsonDict);
            
            //            parsedItems = [[NSMutableArray alloc]init];
            for (id result in jsonArray)
            {
                [parsedItems addObject:result];
            }
            
            [self.tableView reloadData];
            self.title= _navTitle;
            [self loadMoreCompleted];
            [self refreshCompleted];
        }
        
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    if (parsedItems == nil) {
        parsedItems = [[NSMutableArray alloc] init];
    }
}

-(void)fetchdata:(NSString *)withCount IntValue:(int) setValue{
    //todo: verify (in addtion; can first condition be removed?
    if ((pageToken == nil || [pageToken isEqualToString:@""]) && parsedItems == nil) {
        parsedItems = [[NSMutableArray alloc]init];
        return;
    }
    NSString *str;
    
    str=[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=%@&maxResults=%i&key=%@&pageToken=%@",urlString,15,YOUTUBE_CONTENT_KEY,pageToken];
    //    str=[NSString stringWithFormat:@"https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId=%@&maxResults=%i&key=%@&pagetoken=%@",urlString,count,YOUTUBE_CONTENT_KEY,pageToken];
    //    NSLog(@"%@", str);
    NSURL *url = [[NSURL alloc]initWithString:[str stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *req = [[NSMutableURLRequest alloc]initWithURL:url];
    
    
    [NSURLConnection sendAsynchronousRequest:req queue:[NSOperationQueue mainQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        if (data == nil) {
            [[[UIAlertView alloc]initWithTitle:@"Error" message:NO_CONNECTION_TEXT delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
            return ;
        }
        else  {
            jsonDict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&connectionError];
            NSArray *jsonArray = [jsonDict valueForKey:@"items"];
            pageToken=[jsonDict valueForKey:@"nextPageToken"];
            
            for (id result in jsonArray)
            {
                [parsedItems addObject:result];
            }
            
            [self.tableView reloadData];
            self.title = _navTitle;
            [self loadMoreCompleted];
            [self refreshCompleted];
        }
        
    }];
    
}

- (void) pinHeaderView
{
    [super pinHeaderView];
    
    // do custom handling for the header view
    AssistTableHeaderView *hv = (AssistTableHeaderView *)self.headerView;
    [hv.activityIndicator startAnimating];
    hv.title.text = @"Loading...";
}

- (void) unpinHeaderView
{
    [super unpinHeaderView];
    
    // do custom handling for the header view
    [[(AssistTableHeaderView *)self.headerView activityIndicator] stopAnimating];
}

- (void) headerViewDidScroll:(BOOL)willRefreshOnRelease scrollView:(UIScrollView *)scrollView
{
    AssistTableHeaderView *hv = (AssistTableHeaderView *)self.headerView;
    if (willRefreshOnRelease)
        hv.title.text = @"Release to refresh...";
    else
        hv.title.text = @"Pull down to refresh...";
}

- (BOOL) refresh
{
    if (![super refresh])
        return NO;
    
    // Do your async call here
    // This is just a dummy data loader:
    [self performSelector:@selector(addItemsOnTop) withObject:nil afterDelay:2.0];
    // See -addItemsOnTop for more info on how to finish loading
    return YES;
}

- (void) willBeginLoadingMore
{
    AssistTableFooterView *fv = (AssistTableFooterView *)self.tableView.tableFooterView;
    [fv.activityIndicator startAnimating];
}

- (void) loadMoreCompleted
{
    [super loadMoreCompleted];
    
    AssistTableFooterView *fv = (AssistTableFooterView *)self.tableView.tableFooterView;
    [fv.activityIndicator stopAnimating];
    
    if (!self.canLoadMore) {
        // Do something if there are no more items to load
        
        // Just show a textual info that there are no more items to load
        fv.infoLabel.hidden = NO;
    }
}

- (BOOL) loadMore
{
    if (![super loadMore])
        return NO;
    
    // Do your async loading here
    [self performSelector:@selector(addItemsOnBottom) withObject:nil afterDelay:2.0];
    // See -addItemsOnBottom for more info on what to do after loading more items
    
    return YES;
}

- (void) addItemsOnTop
{
    [self fetchdata:urlString IntValue:15];
    
    // Call this to indicate that we have finished "refreshing".
    // This will then result in the headerView being unpinned (-unpinHeaderView will be called).
    
}

- (void) addItemsOnBottom
{
    count = count + 15;
    [self fetchdata:urlString IntValue:count];
    
    NSString *totalC=[NSString stringWithFormat:@"%@",[[jsonDict valueForKey:@"pageInfo"]valueForKey:@"totalResults"]];
    
    int totalcount= (int)[totalC integerValue];
    
    if (parsedItems.count > totalcount)
        self.canLoadMore = NO; // signal that there won't be any more items to load
    else
        self.canLoadMore = YES;
    
    // Inform STableViewController that we have finished loading more items
}

//- (NSString *) createRandomValue
//{
//    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
//    [dateFormatter setTimeStyle:NSDateFormatterMediumStyle];
//    
//    return [NSString stringWithFormat:@"%@ %@", [dateFormatter stringFromDate:[NSDate date]],
//            [NSNumber numberWithInt:rand()]];
//}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"%lu", (unsigned long)parsedItems.count);
    if (parsedItems == nil) {
        return 0;
    }
    return parsedItems.count;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AssistTableCell *cell;
    
    if (indexPath.row == 0 && [parsedItems count] > 0) {
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"HeaderCell" forIndexPath:indexPath];

        NSString *url = [[[[[parsedItems objectAtIndex:indexPath.row] valueForKey:@"snippet"] valueForKey:@"thumbnails"] valueForKey:@"high"] valueForKey:@"url"];
        
        [cell.image sd_setImageWithURL:[NSURL URLWithString:url]
                   placeholderImage:[UIImage imageNamed:@"default_placeholder"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];

        cell.lblHeadTitle.text = [[[parsedItems objectAtIndex:indexPath.row] valueForKey:@"snippet"]valueForKey:@"title"];

    } else {
        cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
        if (indexPath.row >= parsedItems.count) {
            NSLog(@"Row requested for inexistent array element %ld, when there are %ld itemsToDisplay", (long)indexPath.row, parsedItems.count);
            return cell;
        }
        
        NSString *title = [[[parsedItems objectAtIndex:indexPath.row] valueForKey:@"snippet"]valueForKey:@"title"];
        cell.lblTitle.text = title;
        
        NSString *datestring = [[[parsedItems objectAtIndex:indexPath.row] valueForKey:@"snippet"] valueForKey:@"publishedAt"];
        
        NSDateFormatter *dateFormatter = [NSDateFormatter new];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"];
        
        NSDate *date11 = [dateFormatter dateFromString:datestring];
        
        [dateFormatter setDateFormat:@"dd-MM-yyyy hh:mm"];
        cell.lblDate.text =[dateFormatter stringFromDate:date11];
        
        [cell.image sd_setImageWithURL:[NSURL URLWithString:[[[[[parsedItems objectAtIndex:indexPath.row] valueForKey:@"snippet"] valueForKey:@"thumbnails"] valueForKey:@"default"] valueForKey:@"url"]]
                     placeholderImage:[UIImage imageNamed:@"youtube.png"] options:indexPath.row == 0 ? SDWebImageRefreshCached : 0];
        cell.image.contentMode = UIViewContentModeScaleAspectFill;
        cell.image.clipsToBounds = YES;
    }
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showDetails"]) {
        NSIndexPath *indexPath = sender;
        
        // Show detail
        NSLog(@"%@",[[NSString stringWithFormat:@"http://www.youtube.com/watch?v=%@",[[[[parsedItems objectAtIndex:indexPath.row] valueForKey:@"snippet"]valueForKey:@"resourceId"] valueForKey:@"videoId"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]);
        
        YoutubeDetailViewController *detailVC = (YoutubeDetailViewController *)segue.destinationViewController;
        detailVC.summary = [[[parsedItems objectAtIndex:indexPath.row] valueForKey:@"snippet"]valueForKey:@"description"];
        detailVC.imageUrl=[[[[[parsedItems objectAtIndex:indexPath.row] valueForKey:@"snippet"] valueForKey:@"thumbnails"] valueForKey:@"high"] valueForKey:@"url"];
        detailVC.titleText=[[[parsedItems objectAtIndex:indexPath.row] valueForKey:@"snippet"]valueForKey:@"title"];
        detailVC.videoUrl=[[NSString stringWithFormat:@"http://www.youtube.com/watch?v=%@",[[[[parsedItems objectAtIndex:indexPath.row] valueForKey:@"snippet"]valueForKey:@"resourceId"] valueForKey:@"videoId"]] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSString *datestring= [[[parsedItems objectAtIndex:indexPath.row] valueForKey:@"snippet"]valueForKey:@"publishedAt"];
        NSDateFormatter *dateFormatter=[NSDateFormatter new];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"];
        NSDate *date11=[dateFormatter dateFromString:datestring];
        detailVC.date =[NSString stringWithFormat:@"%@",date11];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"showDetails" sender:indexPath];
    
    // Deselect
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

@end
