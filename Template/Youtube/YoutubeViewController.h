//
// YoutubeViewController.h
//
// Copyright (c) 2015 Sherdle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STableViewController.h"
#import "UIImageView+WebCache.h"
#import "MWFeedParser.h"

@interface YoutubeViewController : STableViewController {
  
    UIImageView *imgVwCell;
    UILabel *lblCell;

    NSMutableArray *parsedItems;
    UITableView *tableView;
    
    NSDateFormatter *formatter;
    int count;
    NSDictionary *jsonDict;
    NSString *pageToken;

}

@property(strong,nonatomic)NSString *urlString;
@property(strong,nonatomic)NSString *navTitle;

@end
