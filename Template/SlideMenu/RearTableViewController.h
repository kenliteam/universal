//
//  RearTableViewController.h
//
//  Copyright (c) 2015 Sherdle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RearTableViewController : UITableViewController <UIAlertViewDelegate, UITableViewDataSource, UITableViewDelegate> {
    UILabel *lblVertLine;
    UIImageView *cellImgVw;
    UILabel *lblHorizLineBottom;
}

@property (strong, nonatomic) UIView *headerView;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (strong, nonatomic) NSArray *sectionArray;

@end
