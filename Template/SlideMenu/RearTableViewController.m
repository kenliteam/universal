//
//  RearTableViewController.m
//
//  Copyright (c) 2015 Sherdle. All rights reserved.
//

#import "RearTableViewController.h"
#import "RearViewCell.h"
#import "SWRevealViewController.h"
#import "KILabel.h"

#import "RadioViewController.h"
#import "FacebookViewController.h"
#import "MapsViewController.h"
#import "RssViewController.h"
#import "YoutubeViewController.h"
#import "TumblrViewController.h"
#import "WebViewController.h"
#import "WordpressViewController.h"
#import "InstagramViewController.h"
#import "TwitterViewController.h"

#import "Config.h"

#import "AppDelegate.h"

#import "FrontNavigationController.h"


@interface RearTableViewController ()

@end

@implementation RearTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor clearColor];
    
    Config * obje = [[Config  alloc]init];
    
    self.selectedIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    self.sectionArray = [obje config];
    
    if (![ABOUT_TEXT isEqual: @""]){
    
        UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    
        [button setTitle:@"About" forState:UIControlStateNormal];
        [button setTitleColor: [UIColor whiteColor] forState:UIControlStateNormal];
        [button sizeToFit];
        button.center = CGPointMake(-10, 0);
        [button addTarget:self
                      action:@selector(launchAbout:)
            forControlEvents:UIControlEventTouchDown];
        self.tableView.tableFooterView = button;
    }
}

- (void) launchAbout:(UIButton *)paramSender{
    [[[UIAlertView alloc]initWithTitle:@"About"
                                message:ABOUT_TEXT delegate:self
                                cancelButtonTitle:@"Ok"
                                otherButtonTitles:@"Support", nil]show];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
     if (buttonIndex == [alertView firstOtherButtonIndex]){
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:ABOUT_URL]];
     }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

-(void)myItemsClicked{
}

-(void)settingBtnClicked {
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    //NSLog(@"The number is: %lu", (unsigned long)self.sectionArray.count);
    return self.sectionArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    NSMutableArray *sec = [self.sectionArray objectAtIndex: section];
    
    return (sec.count - 1);
}

// item view
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RearViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    NSArray *item = [[self.sectionArray objectAtIndex: indexPath.section] objectAtIndex:(indexPath.row+1)];
    
    cell.textLabel.text = [item objectAtIndex: 0];
    
    if ([item count] > 4 && [item objectAtIndex: 4] != nil) {
        cell.imageView.image = [UIImage imageNamed:[item objectAtIndex: 4]];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([indexPath isEqual: self.selectedIndexPath]) {
        cell.backgroundColor = SELECTED_COLOR;
    } else {
        cell.backgroundColor = [UIColor clearColor];
    }
    cell.textLabel.backgroundColor = [UIColor clearColor];
}

//table head view
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    _headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(15, 0, 300, 40)];
    lbl.backgroundColor = [UIColor clearColor];
    lbl.font = [UIFont systemFontOfSize:18];
    lbl.textColor = [UIColor lightTextColor];
    NSMutableArray *sec = [self.sectionArray objectAtIndex: section];
    lbl.text = [sec objectAtIndex:0];
    
    [_headerView addSubview:lbl];
    
    return _headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([[[self.sectionArray objectAtIndex: section] objectAtIndex:0]  isEqual: @""])
        return CGFLOAT_MIN;
    
    return 35;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showFeed"]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        FrontNavigationController *frontNav = (FrontNavigationController *)segue.destinationViewController;
        frontNav.selectedIndexPath = indexPath;
        
        RearTableViewController *rearVC = (RearTableViewController *)segue.sourceViewController;
        NSIndexPath *oldIndexPath = rearVC.selectedIndexPath;
        rearVC.selectedIndexPath = indexPath;
        
        [self.revealViewController revealToggle:nil];
        [self.tableView reloadRowsAtIndexPaths:@[oldIndexPath, indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}

@end
