//
//  RearViewCell.m
//  Universal
//
//  Created by Mu-Sonic on 07/11/2015.
//  Copyright © 2015 Sherdle. All rights reserved.
//

#import "RearViewCell.h"

@implementation RearViewCell

- (void)awakeFromNib {
    // Initialization code
//    [self.contentView addSubview:lblHorizLineBottom];
    
//    UIImageView *cellImgVw = [[UIImageView alloc]initWithFrame:CGRectMake(220, 10, 24, 24)];
//    //TODO no need to transform every image if we can use a white png
//    cellImgVw.image = [self overlayImage:[UIImage imageNamed:@"forward-512.png"] withColor:[UIColor whiteColor]];
//    cellImgVw.alpha = 0.5;
//    [self.contentView addSubview:cellImgVw];
    // for accessoryType to appear instead of the above
    // the RevealController.rearViewRevealOverdraw has to be 0
    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
//    cell.textLabel.textColor = [UIColor whiteColor];
//    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16];

    UIView *bgColorView = [[UIView alloc] init];
    bgColorView.backgroundColor = SELECTED_COLOR;
    [self setSelectedBackgroundView:bgColorView];
}

- (void)prepareForReuse {
    self.imageView.image = nil;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (UIImage *)overlayImage:(UIImage *)image withColor:(UIColor *)color
{
    //  Create rect to fit the PNG image
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    
    //  Start drawing
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    //  Fill the rect by the final color
    [color setFill];
    CGContextFillRect(context, rect);
    
    //  Make the final shape by masking the drawn color with the images alpha values
    CGContextSetBlendMode(context, kCGBlendModeDestinationIn);
    [image drawInRect: rect blendMode:kCGBlendModeDestinationIn alpha:1];
    
    //  Create new image from the context
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    
    //  Release context
    UIGraphicsEndImageContext();
    
    return img;
}

@end
