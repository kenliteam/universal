//
//  WebViewController.h
//
//  Copyright (c) 2015 Sherdle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewController : UIViewController<UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webView;

@property(strong,nonatomic)NSString *urlString;
@property(strong,nonatomic)NSString *navTitle;

@end
