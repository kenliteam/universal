//
//  WebViewController.m
//
//  Copyright (c) 2015 Sherdle. All rights reserved.
//

#import "WebViewController.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "CommonBanner.h"

@implementation WebViewController
@synthesize urlString;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (ADS_ON)
        self.canDisplayAds = YES;
    
    self.title = @"Loading...";
    
    _webView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}

- (IBAction)goForward:(id)sender {
    [_webView goForward];
}

- (IBAction)goBack:(id)sender {
    [_webView goBack];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    self.title=@"Loading...";
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    self.title= _navTitle;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    if (error.code == NSURLErrorNotConnectedToInternet){
        [[[UIAlertView alloc]initWithTitle:@"Error" message:NO_CONNECTION_TEXT delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil]show];
    }
    
    self.title = _navTitle;
}

- (void)loadRequest:(NSURLRequest *)request
{
    if ([_webView isLoading])
        [_webView stopLoading];
    [_webView loadRequest:request];
}

- (void)viewWillDisappear
{
    if ([_webView  isLoading])
        [_webView  stopLoading];
}

@end
