//
//  InstagramViewController.h
//
//  Copyright (c) 2015 Sherdle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardCell.h"
#import "SocialFetcher.h"
#import "UIImageView+WebCache.h"
#import "STableViewController.h"

@interface InstagramViewController : STableViewController <CardCellDelegate, UITableViewDataSource, UITableViewDelegate>

@property(strong,nonatomic)NSString *urlString;
@property(strong,nonatomic)NSString *navTitle;
@property(strong,nonatomic)NSMutableArray *postItems;

@end
