//
//  UIViewController+PresentActions.h
//  Universal
//
//  Created by Mu-Sonic on 13/11/2015.
//  Copyright © 2015 Sherdle. All rights reserved.
//

@interface UIViewController (PresentActions)

- (void)presentActions:(NSArray *)activityItems sender:(id)sender;

@end

