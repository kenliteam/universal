//
//  Config.m
//
//  Copyright (c) 2015 Sherdle. All rights reserved.
//
//  INFO: From this file you can edit your app's main content.
//

#import "Config.h"

@implementation Config {
    
}

- (NSArray *)config {
    NSMutableArray *sections = [NSMutableArray array];
    
    //-- Please only edit below this line.
    
    [sections addObject: @[@"Section",
                           @[@"Recent Posts", @"WORDPRESS", @"", @"http://www.androidpolice.com/api/"],
                           @[@"Videos", @"YOUTUBE", @"UUK8sQmJBp8GCxrOtXWBpyEA", @""],
                           @[@"Twitter", @"TWITTER", @"google", @""],
                           @[@"Facebook", @"FACEBOOK", @"104958162837", @""],
                           @[@"Instagram", @"INSTAGRAM", @"1067259270", @""],
                           @[@"News Feed", @"RSS", @"http://feeds.feedburner.com/OfficialAndroidBlog", @""],
                           ]];
    
    [sections addObject: @[@"Other Section",
                           @[@"Tumblr", @"TUMBLR", @"android-backgrounds", @"API"],
                           @[@"BBC1 Radio", @"RADIO", @"http://www.listenlive.eu/bbcradio1.m3u", @"API"],
                           @[@"Jokes", @"WEB", @"https://en.wikipedia.org/?title=Joke", @"API"],
                           @[@"Map", @"MAPS", @[@"-33.84",@"151.21",@"14"], @[@"Event Location", @"This is where the super cool event takes place."]],
                           ]];
    
    //---- Please only edit above this line
    
    return [NSArray arrayWithArray:sections];
}
@end