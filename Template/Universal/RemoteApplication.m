//
//  RemoteApplication.m
//  Universal
//
//  Created by Mark on 05-12-15.
//  Copyright © 2015 Sherdle. All rights reserved.
//

#import "RemoteApplication.h"
#import "AppDelegate.h"
#import <MediaPlayer/MPNowPlayingInfoCenter.h>


@implementation RemoteApplication

- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    if (event.type == UIEventTypeRemoteControl) {
        AppDelegate* appDelegate = (AppDelegate*) [[UIApplication sharedApplication] delegate];
        
        NSLog(@"Remove Event Received");
        if(event.type == UIEventTypeRemoteControl)
        {
            switch(event.subtype)
            {
                case UIEventSubtypeRemoteControlPause:
                    [appDelegate.player pause];
                case UIEventSubtypeRemoteControlStop:
                    break;
                case UIEventSubtypeRemoteControlPlay:
                    [appDelegate.player play];
                default:
                    break;
            }
        
            NSDictionary *songInfo = [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo;
            NSMutableDictionary *mutableSongInfo = [songInfo mutableCopy];
            [mutableSongInfo setObject: [NSNumber numberWithDouble:appDelegate.player.rate] forKey: MPNowPlayingInfoPropertyPlaybackRate];
            
            [[MPNowPlayingInfoCenter defaultCenter] setNowPlayingInfo:mutableSongInfo];
            
        }
    }
}


-(BOOL)canBecomeFirstResponder {
    return YES;
}

@end
