//
//  Config.h
//
//  Copyright (c) 2015 Sherdle. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Config : NSObject

- (NSArray *)config;
@end