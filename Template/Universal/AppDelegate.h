//
//  AppDelegate.h
//
//  Copyright (c) 2015 Sherdle. All rights reserved.
//
//  INFO: In this file you can edit some of your apps main properties, like API keys
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "RearTableViewController.h"
#import <AVFoundation/AVFoundation.h>

#define APP_THEME_COLOR [UIColor colorWithRed:51.0f/255 green:180.0f/255 blue:227.0f/255 alpha:1.0]
#define MENU_BACKGROUND_COLOR_1 [UIColor colorWithRed:(120/255.0) green:(135/255.0) blue:(150/255.0) alpha:1.0]
#define MENU_BACKGROUND_COLOR_2 [UIColor colorWithRed:(67/255.0)  green:(89/255.0)  blue:(106/255.0)  alpha:1.0]

#define NO_CONNECTION_TEXT @"We weren't able to connect to the server. Make sure you have a working internet connection."
#define ABOUT_TEXT @"Thank you for downloading our app! \n\nIf you need any help, press the button below to visit our support."
#define ABOUT_URL @"https://dashboard.heroku.com/apps/obscure-depths-26601/resources"

#define ADS_ON false
#define ADMOB_UNIT_ID @""

#define MAPS_API_KEY @"PLACEHOLDER"

#define YOUTUBE_CONTENT_KEY @"PLACEHOLDER"

#define TWITTER_API @"iOixdOWc7H3kIzb9o8L5qrx2p"
#define TWITTER_API_SECRET @"PwsT8oruKKMquayanHyOTqFBsbgLqEO9eMANTBg6faXCTjCcj2"
#define TWITTER_TOKEN @"2923526202-Bff4gBJOXJtXqZ6qxaByvH6FiDUFY3J0S4vb3NQ"
#define TWITTER_TOKEN_SECRET @"fHrKIYaOjmybAjT7pLeGk6i1rPxBqTeCTJR31utbxrC47"

#define FACEBOOK_ACCESS_TOKEN @"453160674836468|gQsMkIIq80xXmlBsNwsZVB2Afg0"
#define INSTAGRAM_CLIENT_ID @"b6131c9e7bbd43b4b2e057e5e8d64e9e"

@interface AppDelegate : UIResponder <UIApplicationDelegate>  {
    SWRevealViewController *revealController;
    RearTableViewController *rearViewController;
    
    UINavigationController *frontNav;
    UINavigationController *rearNav;
}

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, retain) AVPlayer *player;

@end
