//
//  MapsViewController.m
//
//  Copyright (c) 2015 Sherdle. All rights reserved.
//

#import "MapsViewController.h"
#import "SWRevealViewController.h"
#import <MapKit/MapKit.h>
#import "CommonBanner.h"
#import "AppDelegate.h"

@implementation MapsViewController
{
    IBOutlet GMSMapView *mapView_;
}

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (ADS_ON)
        self.canDisplayAds = YES;
    
    NSArray* coordinatesArray = _urlString;
    NSArray* detailsArray = _mainURL;
    NSString* lat = [coordinatesArray objectAtIndex:0];
    NSString* lon = [coordinatesArray objectAtIndex:1];
    NSString* zoom = [coordinatesArray objectAtIndex:2];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[lat doubleValue]
                                                            longitude:[lon doubleValue]
                                                                 zoom:[zoom doubleValue]];
    mapView_.myLocationEnabled = YES;

    //[self.view addSubview:mapView_];
    
    // Creates a marker in the center of the map.
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake([lat doubleValue], [lon doubleValue]);
    marker.title = [detailsArray objectAtIndex:0];
    marker.snippet = [detailsArray objectAtIndex:1];
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = mapView_;
    
    [mapView_ setSelectedMarker:marker];
    
    mapView_.camera = camera;
    
    self.title = _navTitle;
}

- (IBAction)navigateTo {
    NSArray* coordinatesArray = _urlString;
    NSString* lat = [coordinatesArray objectAtIndex:0];
    NSString* lon = [coordinatesArray objectAtIndex:1];
    
    Class mapItemClass = [MKMapItem class];
    if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
    {
        // Create an MKMapItem to pass to the Maps app
        CLLocationCoordinate2D coordinate =
        CLLocationCoordinate2DMake([lat doubleValue], [lon doubleValue]);
        MKPlacemark *placemark = [[MKPlacemark alloc] initWithCoordinate:coordinate
                                                       addressDictionary:nil];
        MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
        [mapItem setName:[_mainURL objectAtIndex:0]];
        // Pass the map item to the Maps app
        [mapItem openInMapsWithLaunchOptions:nil];
    }
}

//- (void)viewWillDisappear:(BOOL)animated{
//    [super viewWillDisappear:animated] ;
//    [mapView_ clear];
//    [mapView_ removeFromSuperview] ;
//    mapView_ = nil ;
//    self.view=nil;
//}

//- (void)dealloc{
//    [mapView_ clear];
//    [mapView_ removeFromSuperview] ;
//    mapView_ = nil ;
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
