//
//  MapsViewController.h
//
//  Copyright (c) 2015 Sherdle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface MapsViewController : UIViewController

@property(strong,nonatomic)NSArray *urlString;
@property(strong,nonatomic)NSArray *mainURL;
@property(strong,nonatomic)NSString *navTitle;

@end
