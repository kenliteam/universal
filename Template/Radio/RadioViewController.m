//
//  RadioViewController.m
//
//  Copyright (c) 2015 Sherdle. All rights reserved.
//

#import "RadioViewController.h"
#import "SWRevealViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import <MediaPlayer/MPMediaItem.h>
#import <MediaPlayer/MPNowPlayingInfoCenter.h>
#import "UIImageView+WebCache.h"
#import "CommonBanner.h"

@interface RadioViewController ()

@end

@implementation RadioViewController
@synthesize urlString;
AppDelegate *appDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Configure Ads
    if (ADS_ON)
        self.canDisplayAds = YES;
    
    //Set the Sliding Menu listeners
    [self.view addGestureRecognizer: self.revealViewController.panGestureRecognizer];
    [self.view addGestureRecognizer: self.revealViewController.tapGestureRecognizer];
    
    //Configure the layout
    _imageVieewBackground.layer.cornerRadius=8;
    self.title = _navTitle;
    
    [self configureAudioSession];
    
    //Allows us to update the player controls when the app is resumed
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];

    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //Initialize player items to be used when playback starts
    _playerItem=[AVPlayerItem alloc];
    _playerItem = [AVPlayerItem playerItemWithURL:[NSURL URLWithString:urlString]];
    
    //Set the volume slider changed listener
    [self volumeSliderChanged:self];
    
}

//TODO perhaps also do this for applicationWillEnterBackground
- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self updatePlayerControls];
}

-(void)viewWillAppear:(BOOL)animated{
    [self updatePlayerControls];
}

-(void)viewWillDisappear:(BOOL)animated{

}

-(void)viewDidDisappear:(BOOL)animated{
}

- (void) updatePlayerControls{
    //Set buttons to correct state after opening the radio
    if (appDelegate.player.rate > 0){
        [self setPlayingLayout];
    } else {
        [self setPausedLayout];
    }
}

- (IBAction)volumeSliderChanged:(id)sender {
    appDelegate.player.volume = self.volumeSlider.value;
    self.volumeLabel.text = [NSString stringWithFormat:@"%d%%", (int)round(self.volumeSlider.value * 100)];
}

-(NSString *)urlOfCurrentlyPlayingInPlayer:(AVPlayer *)player{
    // get current asset
    AVAsset *currentPlayerAsset = player.currentItem.asset;
    // make sure the current asset is an AVURLAsset
    if (![currentPlayerAsset isKindOfClass:AVURLAsset.class]) return nil;
    // return the NSURL
    return [[(AVURLAsset *)currentPlayerAsset URL] absoluteString];
}

- (IBAction)btnplayclicked:(id)sender {
    NSError *error;
    
    //Initialize new player if it does not yet exist, or if it is a player for a different url;
    if (appDelegate.player == nil &&
        ![[self urlOfCurrentlyPlayingInPlayer:appDelegate.player] isEqualToString: urlString]){
        
        _indicator.hidden = false;
        
        appDelegate.player = [AVPlayer playerWithPlayerItem:_playerItem];
        appDelegate.player = [AVPlayer playerWithURL:[NSURL URLWithString:urlString]];
        
        // Declare block scope variables to avoid retention cycles
        // from references inside the block
        __block AVPlayer* blockPlayer = appDelegate.player;
        __weak UIActivityIndicatorView* indicator = _indicator;
        __weak UIImageView *playerView = _playerView;
        __block id obs;
        
        // Setup boundary time observer to trigger when audio really begins,
        // specifically after 1/3 of a second playback
        obs = [appDelegate.player addBoundaryTimeObserverForTimes:
               @[[NSValue valueWithCMTime:CMTimeMake(1, 3)]]
                                                            queue:NULL
                                                       usingBlock:^{
                                                           
                                                           [playerView sd_setImageWithURL:[[NSBundle mainBundle] URLForResource:@"equalizer" withExtension:@"gif"]];
                                                           [indicator removeFromSuperview];
                                                           // Remove the boundary time observer
                                                           [blockPlayer removeTimeObserver:obs];
                                                       }];
    } else {
        [_playerView sd_setImageWithURL:[[NSBundle mainBundle] URLForResource:@"equalizer" withExtension:@"gif"]];
    }

    //Make sure the player is not null, if this is the case, start playing
    if (appDelegate.player == nil)
        NSLog(@"%@", [error description]);
    else
        [appDelegate.player play];
    
    [self setPlayingLayout];
    
    //Register for remove control events //TODO does not work for other view controllers within app, only for background
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
}

- (IBAction)btnpauseclicked:(id)sender {
    [appDelegate.player pause];
    [self setPausedLayout];
    
    //Unregister for remove control events
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    [self resignFirstResponder];
}

- (void) setPlayingLayout {
    [_btnPlay setBackgroundImage:[UIImage imageNamed:@"ic_play_active.png"] forState:UIControlStateNormal];
    [_btnPause setBackgroundImage:[UIImage imageNamed:@"ic_pause_unactive.png"] forState:UIControlStateNormal];
    
    _playerView.hidden = false;
    
    //Set the playing info for the Control Center
    [self setMPNowPlayingInfoCenterInfo: @"Live Streaming" : _navTitle];
}

- (void) setPausedLayout {
    [_btnPause setBackgroundImage:[UIImage imageNamed:@"ic_pause_active.png"] forState:UIControlStateNormal];
    [_btnPlay setBackgroundImage:[UIImage imageNamed:@"ic_play_unactive.png"] forState:UIControlStateNormal];
    
    _playerView.hidden = true;
    
    //In the emulator, the MPNowPlayingInfo does not dissapear, so we'll manually set it to null;
    [self setMPNowPlayingInfoCenterInfo: @"" : @""];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) setMPNowPlayingInfoCenterInfo:(NSString *) title :(NSString *) artist{
    [MPNowPlayingInfoCenter defaultCenter].nowPlayingInfo = @{
            MPMediaItemPropertyTitle : title,
            MPMediaItemPropertyArtist : artist,
            MPNowPlayingInfoPropertyPlaybackRate:[NSNumber numberWithDouble:appDelegate.player.rate],
     };
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}


- (void)configureAudioSession {
    NSError *error;
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&error];
    [[AVAudioSession sharedInstance] setActive:YES error:&error];
    if (error) {
        NSLog(@"Error setting category: %@", [error description]);
    }
}

-(void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


@end
