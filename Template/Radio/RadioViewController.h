//
//  RadioViewController.h
//
//  Copyright (c) 2015 Sherdle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface RadioViewController : UIViewController<AVAudioPlayerDelegate>

- (IBAction)btnplayclicked:(id)sender;
- (IBAction)btnpauseclicked:(id)sender;

@property(strong,nonatomic)NSString *urlString;
@property(strong,nonatomic)NSString *navTitle;

@property (strong, nonatomic) IBOutlet UIImageView *imageVieewBackground;

@property (strong, nonatomic) IBOutlet UIButton *btnPlay;

@property (strong, nonatomic) IBOutlet UIButton *btnPause;

@property (strong, nonatomic) IBOutlet UILabel *volumeLabel;
@property (weak, nonatomic) IBOutlet UISlider *volumeSlider;

@property(nonatomic , strong) AVPlayerItem *playerItem;

@property (weak, nonatomic) IBOutlet UIImageView *playerView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@end
